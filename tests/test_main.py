from main import print_hi, print_hi_to_dev


def test_print_hi():
    assert print_hi('name') is None


def test_print_hi_to_dev(mocker):
    mock_print_hi = mocker.patch('main.print_hi')

    print_hi_to_dev()

    mock_print_hi.assert_called_with('dev')
