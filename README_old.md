# State 
[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=ldr)](https://sonarcloud.io/dashboard?id=ldr)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=ldr)

| Status      | Description |
| ----------- | ----------- |
| [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=coverage)](https://sonarcloud.io/dashboard?id=ldr)   | &#8593; More = better        |
| [![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=bugs)](https://sonarcloud.io/dashboard?id=ldr)      | &#8595; Lower = better  |
| [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=code_smells)](https://sonarcloud.io/dashboard?id=ldr)   | &#8595; Lower = better |
| [![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=ldr)   | &#8595; Lower = better |


# Playground

This is the place to test git, gitlab, python, Sonar and many more features.

## Requirements

The project runs on Python.
It's been tested on version 3.7

It uses poetry for dependencies management.

Make sure you have Python3.7 installed on your system

## Installation

### Clone repository
```shell
git clone git@gitlab.com:eldiiar.egemberdiev/playground.git
```

### Go to the project folder 
```shell
cd playgound
```

### Initialize and activate virtual environment 

In most cases you want to develop python programs using virtual environment.
You can use official [docs](https://docs.python.org/3/library/venv.html) 
to get started

### Install poetry

This project uses [poetry](https://python-poetry.org/docs/#installation) as main dependencies management tool

### Install dependencies
This will install all dependencies in your virtual environment
```shell
poetry install
```

# Run tests

If you simply want to run tests do:

```shell
pytest
```

## Run coverage

The project uses [coverage](https://coverage.readthedocs.io/en/coverage-5.5/)
to produce coverage reports.
[pytest-cov](https://pytest-cov.readthedocs.io/en/latest/) plugin is in place for convenience

Here are different ways you can run tests coverage 

This will run tests and display report in the terminal
```shell
pytest --cov=.
```

This will run tests and put report data in the html. 
You can find it in coverage-reports. 
Simply open `coverage-reports/index.html` file in your browser
and you should be able to see the report

```shell
pytest --cov=. --cov-report html
```
