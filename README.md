# State 
[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=ldr)](https://sonarcloud.io/dashboard?id=ldr)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=ldr)

| Status      | Description |
| ----------- | ----------- |
| [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=coverage)](https://sonarcloud.io/dashboard?id=ldr)   | &#8593; More = better        |
| [![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=bugs)](https://sonarcloud.io/dashboard?id=ldr)      | &#8595; Lower = better  |
| [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=code_smells)](https://sonarcloud.io/dashboard?id=ldr)   | &#8595; Lower = better |
| [![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=ldr&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=ldr)   | &#8595; Lower = better |


# Playground

This is the place to test git, gitlab, python, Sonar and many more features.

## Requirements

The project runs on Python.
It's been tested on version 3.8

It uses poetry for dependencies management.

Make sure you have Python3.7 installed on your system

## Installation

### Clone repository
```shell
git clone git@gitlab.com:eldiiar.egemberdiev/playground.git
```

### Go to the project folder 
```shell
cd playgound
```

### Initialize and activate virtual environment 

In most cases you want to develop python programs using virtual environment.
You can use official [docs](https://docs.python.org/3/library/venv.html) 
to get started

### Install poetry

This project uses [poetry](https://python-poetry.org/docs/#installation) as main dependencies management tool

### Install dependencies
This will install all dependencies in your virtual environment
```shell
poetry install
```

# Run tests

If you simply want to run tests do:

```shell
pytest
```

## Run coverage

The project uses [coverage](https://coverage.readthedocs.io/en/coverage-5.5/)
to produce coverage reports.
[pytest-cov](https://pytest-cov.readthedocs.io/en/latest/) plugin is in place for convenience

Here are different ways you can run tests coverage 

This will run tests and display report in the terminal
```shell
pytest --cov=.
```

This will run tests and put report data in the html. 
You can find it in coverage-reports. 
Simply open `coverage-reports/index.html` file in your browser
and you should be able to see the report

```shell
pytest --cov=. --cov-report html
```

# Cloud Run Hello World with Cloud Code

"Hello World" is a [Cloud Run](https://cloud.google.com/run/docs) application that renders a simple webpage.

For details on how to use this sample as a template in Cloud Code, read the documentation for Cloud Code for [VS Code](https://cloud.google.com/code/docs/vscode/quickstart-cloud-run?utm_source=ext&utm_medium=partner&utm_campaign=CDR_kri_gcp_cloudcodereadmes_012521&utm_content=-) or [IntelliJ](https://cloud.google.com/code/docs/intellij/quickstart-cloud-run?utm_source=ext&utm_medium=partner&utm_campaign=CDR_kri_gcp_cloudcodereadmes_012521&utm_content=-).

### Table of Contents
* [Getting Started with VS Code](#getting-started-with-vs-code)
* [Getting Started with IntelliJ](#getting-started-with-intellij)
* [Sign up for User Research](#sign-up-for-user-research)

---
## Getting Started with VS Code

### Run the app locally with the Cloud Run Emulator
1. Click on the Cloud Code status bar and select 'Run on Cloud Run Emulator'.  
![image](./src/img/status-bar.png)

2. Use the Cloud Run Emulator dialog to specify your [builder option](https://cloud.google.com/code/docs/vscode/deploying-a-cloud-run-app#deploying_a_cloud_run_service). Cloud Code supports Docker, Jib, and Buildpacks. See the skaffold documentation on [builders](https://skaffold.dev/docs/pipeline-stages/builders/) for more information about build artifact types.  
![image](./src/img/build-config.png)

3. Click ‘Run’. Cloud Code begins building your image.

4. View the build progress in the OUTPUT window. Once the build has finished, click on the URL in the OUTPUT window to view your live application.  
![image](./src/img/cloud-run-url.png)

5. To stop the application, click the stop icon on the Debug Toolbar.

---
## Getting Started with IntelliJ

### Run the app locally with the Cloud Run Emulator

#### Define run configuration

1. Click the Run/Debug configurations dropdown on the top taskbar and select 'Edit Configurations'.  
![image](./src/img/edit-config.png)

2. Select 'Cloud Run: Run Locally' and specify your [builder option](https://cloud.google.com/code/docs/intellij/developing-a-cloud-run-app#defining_your_run_configuration). Cloud Code supports Docker, Jib, and Buildpacks. See the skaffold documentation on [builders](https://skaffold.dev/docs/pipeline-stages/builders/) for more information about build artifact types.  
![image](./src/img/local-build-config.png)

#### Run the application
1. Click the Run/Debug configurations dropdown and select 'Cloud Run: Run Locally'. Click the run icon.  
![image](./src/img/config-run-locally.png)

2. View the build process in the output window. Once the build has finished, you will receive a notification from the Event Log. Click 'View' to access the local URLs for your deployed services.  
![image](./src/img/local-success.png)

---
## Sign up for User Research

We want to hear your feedback!

The Cloud Code team is inviting our user community to sign-up to participate in Google User Experience Research. 

If you’re invited to join a study, you may try out a new product or tell us what you think about the products you use every day. At this time, Google is only sending invitations for upcoming remote studies. Once a study is complete, you’ll receive a token of thanks for your participation such as a gift card or some Google swag. 

[Sign up using this link](https://google.qualtrics.com/jfe/form/SV_4Me7SiMewdvVYhL?reserved=1&utm_source=In-product&Q_Language=en&utm_medium=own_prd&utm_campaign=Q1&productTag=clou&campaignDate=January2021&referral_code=UXbT481079) and answer a few questions about yourself, as this will help our research team match you to studies that are a great fit.
